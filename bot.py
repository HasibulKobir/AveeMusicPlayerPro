#!/usr/bin/env python3

import logging
import os
import sys
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler, ConversationHandler)

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger(__name__)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

VOICE, PHOTO, TEXT = range(3)
DOWNLOAD_LOCATION = "./DOWNLOADS"

def start(bot, update):
    update.message.reply_text(
        'Hi! this bot is a clone, and a copycat of https://play.google.com/store/apps/details?id=com.daaw.avee '
        'Send /cancel to stop talking to me.\n\n'
        'First send me any audio file, or simply type @MP3sBot in the text box'
        ' and do not press Enter')

    return VOICE


def voice(bot, update):
    user = update.message.from_user
    audio_file = bot.get_file(update.message.audio.file_id)
    current_file_name = str(user.id) + "_AUDIO"
    audio_file.download(DOWNLOAD_LOCATION + "/" + current_file_name)
    logger.info("Audio of %s: %s", user.first_name, current_file_name)
    update.message.reply_text('Now, send me an image to display in the video! '
                              'or send /skip if you don\'t want to.')
    return PHOTO


def photo(bot, update):
    user = update.message.from_user
    photo_file = bot.get_file(update.message.photo[-1].file_id)
    current_file_name = str(user.id) + "_IMAGE"
    photo_file.download(DOWNLOAD_LOCATION + "/" + current_file_name)
    logger.info("Photo of %s: %s", user.first_name, current_file_name)
    update.message.reply_text('Finally, send me the text to display in the video')

    return TEXT


def skip_photo(bot, update):
    user = update.message.from_user
    logger.info("User %s did not send a photo.", user.first_name)
    update.message.reply_text('A black image will be used! \n\n'
                              'Finally, send me the text to display in the video')

    return TEXT


def bio(bot, update):
    user = update.message.from_user
    logger.info("Bio of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Processing, Please wait ...')
    ## Do the Processing task here
    r = ConvertAudioToVideo(
            DOWNLOAD_LOCATION + "/" + str(user.id) + "_AUDIO",
            DOWNLOAD_LOCATION + "/" + str(user.id) + "_IMAGE",
            update.message.text,
            DOWNLOAD_LOCATION + "/" + str(user.id) + "_OUTPUT",
        )
    update.message.reply_text('Bye! I hope we can talk again some day.')
    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def ConvertAudioToVideo(audio_file, image_file, text_to_display, output_file):
    first_ffmpeg_command = "ffmpeg -i " + audio_file + " -filter_complex \"[0:a]avectorscope=s=1280x720,format=yuv420p[v]\" -map \"[v]\" -map 0:a " + output_file + "_1.mp4"
    print(first_ffmpeg_command)
    ## this returns a big file 100 MB



def main(bot_token):
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(bot_token)
    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    # Add conversation handler with the states
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            VOICE: [MessageHandler(Filters.audio, voice)],

            PHOTO: [MessageHandler(Filters.photo, photo),
                    CommandHandler('skip', skip_photo)],

            TEXT: [MessageHandler(Filters.text, bio)]
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )
    dp.add_handler(conv_handler)
    # log all errors
    dp.add_error_handler(error)
    # Start the Bot
    updater.start_polling()
    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    if(len(sys.argv) != 2):
        print("python3", sys.argv[0], "BOT_TOKEN")
        ConvertAudioToVideo(
            DOWNLOAD_LOCATION + "/" + "7351948_AUDIO",
            DOWNLOAD_LOCATION + "/" + "7351948_IMAGE",
            "text to display",
            DOWNLOAD_LOCATION + "/" + "7351948_OUTPUT",
        )
    else:
        main(sys.argv[1])
